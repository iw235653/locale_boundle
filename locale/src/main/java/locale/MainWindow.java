package locale;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ResourceBundle;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainWindow extends JFrame implements Runnable, ActionListener{

	private static final long serialVersionUID = 1L;
	
	JLabel titleLabel;
	JRadioButton engButton;
	JRadioButton plButton;
	JButton dataButton;
	JPanel loginPanel;
	ButtonGroup languageGroup;
	
	JLabel dollarLabel;
	JLabel poundLabel;
	JLabel euroLabel;
	JLabel francLabel;
	JLabel roubleLabel;
	JLabel kunaLabel;
	
	double dollarValue = 0;
	double poundValue = 0;
	double euroValue = 0;
	double francValue = 0;
	double roubleValue = 0;
	double kunaValue = 0;
	
	private ResourceBundle boundle;
	
	
	public MainWindow() {
		//----------- OKIENKO ------------
		super("Exchanger");
		this.setSize(500, 320);
		this.setLayout(null);
		
		boundle = ResourceBundle.getBundle("TextBoundle", new Locale("pl", "PL"));
	
		//------------- PANELE --------------
		loginPanel = new JPanel();
		loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.Y_AXIS));
		loginPanel.setBounds(0,0,490,90);
		
		
		//----------- LOGIN PANEL ------------
		//adding components
		titleLabel = new JLabel();
		languageGroup = new ButtonGroup();
		engButton = new JRadioButton("ENG");
		plButton = new JRadioButton("PL");
			languageGroup.add(engButton);
			languageGroup.add(plButton);
			plButton.setSelected(true);
		dataButton = new JButton("Fetch data");
		
		dollarLabel = new JLabel();
		poundLabel = new JLabel();
		euroLabel = new JLabel();
		francLabel = new JLabel();
		roubleLabel = new JLabel();
		kunaLabel = new JLabel();
		
		loginPanel.add(titleLabel);
		loginPanel.add(dataButton);
		loginPanel.add(engButton);
		loginPanel.add(plButton);
		
		loginPanel.add(dollarLabel);
		loginPanel.add(poundLabel);
		loginPanel.add(euroLabel);
		loginPanel.add(francLabel);
		loginPanel.add(roubleLabel);
		loginPanel.add(kunaLabel);
		
		
		//alignment
		titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		dataButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		engButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		plButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		dollarLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		poundLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		euroLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		francLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		roubleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		kunaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
				
		//listeners
		dataButton.addActionListener(this);
		engButton.addActionListener(this);
		plButton.addActionListener(this);

		//setting text
		changeTextTitle();
		
		//---------------- END ----------------
		this.add(loginPanel);
		//this.add(gridPanel);
		setContentPane(loginPanel);
		//setContentPane(gridPanel);
		this.setVisible(true);
	}
	

	public void actionPerformed(ActionEvent evt) {
		Object e = evt.getSource();
		if(e == dataButton) {
			getData();
		}
		if(e == engButton) {
			boundle = ResourceBundle.getBundle("TextBoundle", new Locale("en", "US"));
			System.out.println("Boundle.getLocale: "+boundle.getLocale());
			changeTextTitle();
		}
		if(e == plButton) {
			boundle = ResourceBundle.getBundle("TextBoundle", new Locale("pl", "PL"));
			System.out.println("Boundle.getLocale: "+boundle.getLocale());
			changeTextTitle();
		}
		
	}
	
	public void changeTextTitle() {
		titleLabel.setText(boundle.getString("TitleLabel"));
		//addOrderButton.setText(boundle.getString("AddOrderButton"));
		if(dollarValue <=0)
			dollarLabel.setText(boundle.getString("DollarLabel") + ": brak danych");
		else
			dollarLabel.setText(boundle.getString("DollarLabel") + ": " + dollarValue);
		
		if(poundValue <=0)
			poundLabel.setText(boundle.getString("PoundLabel") + ": brak danych");
		else
			poundLabel.setText(boundle.getString("PoundLabel") + ": " + poundValue);
		
		if(euroValue <=0)
			euroLabel.setText(boundle.getString("EuroLabel") + ": brak danych");
		else
			euroLabel.setText(boundle.getString("EuroLabel") + ": " + euroValue);
		
		if(francValue <=0)
			francLabel.setText(boundle.getString("FrancLabel") + ": brak danych");
		else
			francLabel.setText(boundle.getString("FrancLabel") + ": " + francValue);
		
		if(roubleValue <=0)
			roubleLabel.setText(boundle.getString("RoubleLabel") + ": brak danych");
		else
			roubleLabel.setText(boundle.getString("RoubleLabel") + ": " + roubleValue);
		
		if(kunaValue <=0)
			kunaLabel.setText(boundle.getString("KunaLabel") + ": brak danych");
		else
			kunaLabel.setText(boundle.getString("KunaLabel") + ": " + kunaValue);
	}
	
	public void getData() {
		StringBuilder sb = new StringBuilder();

        sb.append("http://api.nbp.pl/api/exchangerates/tables/A/");
        JSONObject json = null;
        
        try {
			json = readJsonFromUrl(sb.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        System.out.println("Udalo sie! Json:");
        System.out.println(json.toString());
//        System.out.println("Dolar: " + json.getString("effectiveDate"));
        
        JSONArray currencies = json.getJSONArray("rates");
        System.out.println("Currencies: " + currencies);
        System.out.println("Dolar: " + currencies.getJSONObject(1).getDouble("mid"));
        System.out.println("Funt: " + currencies.getJSONObject(10).getDouble("mid"));
        System.out.println("Euro: " + currencies.getJSONObject(7).getDouble("mid"));
        System.out.println("Kuna: " + currencies.getJSONObject(18).getDouble("mid"));
        System.out.println("Frank: " + currencies.getJSONObject(9).getDouble("mid"));
        System.out.println("Rubel: " + currencies.getJSONObject(29).getDouble("mid"));
        
        dollarValue = currencies.getJSONObject(1).getDouble("mid");
        poundValue = currencies.getJSONObject(10).getDouble("mid");
        euroValue = currencies.getJSONObject(7).getDouble("mid");
        kunaValue = currencies.getJSONObject(18).getDouble("mid");
        francValue = currencies.getJSONObject(9).getDouble("mid");
        roubleValue = currencies.getJSONObject(29).getDouble("mid");
        
        changeTextTitle();
	}
	
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {

        InputStream is = new URL(url).openStream();
        try{
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONArray json = new JSONArray(jsonText);
//            System.out.println("NOOOOOWYYYYYY: " + json.toString());
//            JSONObject json2 = new JSONObject(jsonText);
            JSONObject json2 = json.getJSONObject(0);
            return json2;
        }finally {
            is.close();
        }
    }
	
	private static String readAll(Reader rd) throws IOException {

        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp=rd.read())!=-1)
        {
            sb.append((char)cp);
        }
        return sb.toString();
    }
	
	public void run() {
	}
}
