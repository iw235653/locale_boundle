## Locale Boundle

Internacjonalizacja

Program okienkowy. Pobiera dane o kursach najpopularniejszych walut i wyświetla je w dwóch wersjach językowych:
- angielskiej
- polskiej

Korzystam z API strony Narodowego Banku Polskiego. 
Dokumentacja:
`http://api.nbp.pl/en.html`
Zapytania HTTP:
`https://www.jokecamp.com/blog/code-examples-api-http-get-json-different-languages/#java`